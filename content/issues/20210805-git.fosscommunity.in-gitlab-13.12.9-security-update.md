---
title: "Gitlab 13.12.9 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-08-05 22:35:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-08-06 00:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated GitLab to 13.12.9 security release.
