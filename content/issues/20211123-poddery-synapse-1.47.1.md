---
title: "Poddery Matrix Synapse 1.47.1"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-11-23 21:45:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-11-24 02:00:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
  - matrix

# Don't change the value below
section: issue
---
We're updating poddery's matrix-synapse to 1.47.1

Update: Service is operational now after 4 hours of downtime
