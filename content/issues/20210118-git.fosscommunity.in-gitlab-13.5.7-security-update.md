---
title: "Gitlab 13.5.7 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-01-18 18:10:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-01-18 19:30:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.5.7 feature + security release.
