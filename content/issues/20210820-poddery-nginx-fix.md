---
title: "Poddery nginx configuration fixes"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-08-20 10:00:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-08-20 15:10:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - matrix
# - xmpp
 - diaspora
# - gitlab
# - website
# - mailing-lists
# - loomio
# - videos
# - planet
# - jitsi

# Don't change the value below
section: issue
---
Details at https://diasp.in/posts/1877344
