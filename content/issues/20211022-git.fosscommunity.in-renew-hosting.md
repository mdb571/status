---
title: "Renew Hosting"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-10-22 03:54:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-10-22 15:54:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab
# Don't change the value below
section: issue
---
