---
title: "videos.fsci.in maintenance"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-03-28 12:00:00
# Status of the issue.Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-03-28 22:03:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - peertube

# Don't change the value below
section: issue
---

Peertube has been upgraded to 3.0 and storage has been increased
