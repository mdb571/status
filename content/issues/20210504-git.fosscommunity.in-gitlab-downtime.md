---
title: "Storage issues"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-05-04 21:23:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-05-04 22:23:00
# You can use: down, disrupted, notice
severity: down
# affected sections (array). Uncomment the affected one's
affected:
- gitlab


# Don't change the value below
section: issue
---
Storage capacity on the server hosting the GitLab instance exhausted
and all services on the server failed. The primary culprit was the log
directory ( `/var/log/gitlab` ) which was at 51 GB.

Truncating the log file still did not bring the service back online.
The service was restored after multiple services including database
server ( postgreSQL ) and caching server ( redis ) were restarted.
