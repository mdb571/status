---
title: "GitLab v14.4.4 security update "
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-12-10 21:16:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-11-28 23:45:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab
# Don't change the value below
section: issue
---
We have updated Gitlab to v14.4.4
