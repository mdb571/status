---
title: "Gitlab 13.2.10 security update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2020-10-12 15:20:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2020-10-12 16:31:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab

# Don't change the value below
section: issue
---
We updated gitlab to 13.2.10 security release.
