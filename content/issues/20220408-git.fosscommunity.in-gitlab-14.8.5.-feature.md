---
title: "GitLab to v14.8.5"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2022-04-08 22:13:25
# Status of the issue. Boolean value: true, false
resolved: true
# resolved date. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2022-04-08 23:50:00
# You can use: down, disrupted, notice
severity: disrupted
# affected sections (array). Uncomment the affected one's
affected:
 - gitlab
# Don't change the value below
section: issue
---
We have updated Gitlab to v14.8.5

