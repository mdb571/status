---
title: "Gitlab 13.12.3 feature update"
# Incident date. format: yyyy-mm-dd hh:mm:ss
date: 2021-06-19 22:20:00
# Status of the issue. Boolean value: true, false
resolved: true
# resolved true. format: yyyy-mm-dd hh:mm:ss
resolvedWhen: 2021-06-20 00:39:00
# You can use: down, disrupted, notice
severity: notice
# affected sections (array). Uncomment the affected one's
affected:
- gitlab


# Don't change the value below
section: issue
---
We have updated GitLab to 13.12.3 feature update.
